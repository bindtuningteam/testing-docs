![general options](../images/classic/14.generaloptions.png)

<p class="alert alert-success">The information displayed by the Stocks Ticker Web Part is the sole responsibility of the service provider in use. BindTuning cannot guarantee that the displayed information is valid nor the availability of the provided services.</p>

___
### Provider

There are two available providers:
    
 - **Iex** - free (US tickers only) - https://iextrading.com/apps/stocks/ 
 - **World Trading Data** - paid subscription that requires an **API Key** - explained below. You can check all the available symbols for this provider <a href="https://www.worldtradingdata.com/search" target="_blank">here</a>.

___
### Symbols Search (IEX only)

Search for a ticker symbol or a ticker/mutualfund name. You can check all the **available symbols** for this provider <a href="https://iexcloud.io/console/" target="_blank">https://iexcloud.io/console/</a> in the top of your page.

___
### API Key (World Trading Data only)

This is where you store the Api Key of your paid subscription.

You can check all the **available stocks** for this provider <a href="https://www.worldtradingdata.com/search" target="_blank">https://www.worldtradingdata.com/search</a>.

You can check all the **available mutual funds** for this provider <a href="https://www.worldtradingdata.com/search/mutualfunds" target="_blank">https://www.worldtradingdata.com/search/mutualfunds</a>.


____
### Market (World Trading Data only)

This option allow you to select beetwen the stock or the mutuals funds. By default the options selected is **stock**.

![general options](../images/classic/16.provider.png)
___
### Pinned Symbol

You can pin **only one ticker**. These will show on a top section. on the zone where the Web Part is shown.

Eg. for Microsoft you should write **msft**. When you add the Web Part to the page this will be set by default.  

___
### Symbols

This is where you can add **multiple tickers** symbols to the sliding zone of the Web Part.

![sticker](../images/modern/02.general.opt.png)