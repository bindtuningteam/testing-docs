For the auto update of the stock prices you have four interval periods:

 - **5 Minutes**
 - **15 Minutes (Default)**
 - **30 Minutes**
 - **1 Hour**

 ![Other Settings.PNG](../images/classic/12.otheroptions.png)