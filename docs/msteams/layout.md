![Messages](../images/modern/03.layout.png)

### Theme

You can choose between two options:

 - **Light**
 - **Dark**


___
### Transparency

You can choose if you want to display the Web Part with a transparent background. This can be useful when you have colored backgrounds in the page. 

<p class="alert alert-success">To avoid some undesired effects: In a page with a dark background the transparency should not be applied along with the Light Theme. Also, in a page with a light background the transparency should not be applied along with the Dark Theme.</p>

___
### Orientation

You can choose between two options for the layout orientation: 

 - **Horizontal (Default)** - This option will show the pinned symbol in the first row and all of the other symbols go to a second row (sliding zone).
 - **Vertical (All Numbers)** - This option will show the pinned symbol on the first row and all of the other symbols go to the other rows (sliding zone). The sliding zone shows one symbol per row and it grows at a maximum of three rows.
 
___
### Show Logo

This an option to show the ticker logo (if available).

___
### Show Name

This option toggles between the display of the ticker's name and the ticker's symbol.

___
### Auto Slide

This option toggles between a manual and automatic slide of the hidden symbols. If you choose the automatic slide an option with three speed modes will be displayed (Animation Speed).
___
###  Animation Speed

The variation can be shown in three ways:

 - **Slow** - the slide will rotate and ~8s before  the initial ticker dissapear
 - **Normal** - the slide will rotate and ~6s before  the initial ticker dissapear
 - **Fast** - the slide will rotate ~4s before  the initial ticker dissapear
___
### Variations in

The variation can be shown in two ways:

 - **Percentage** - the relative variation
 - **Currency** - the absolute variation