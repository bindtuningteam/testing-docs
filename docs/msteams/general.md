On the Web Part Properties panel, you've multiple options which you can edit for diferent configuration of the Web Part.

- [General](./options.md)
- [Layout](./layout.md)
- [Other Settings](./other.md)
- [Web Part Appearance](./appearance.md)
- [Advanced Options](./advanced.md)
- [Web Part Messages](./message.md)
 
![06.options](../images/modern/06.options.png)